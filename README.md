# README #

This repository holds a few very simple Business Basic (with Java) programs written by Steve Swett.  The main purpose of the repository is to allow others to look at some code.  A real "project" has not yet been started.

# DEVELOPMENT ENVIRONMENT #

The Eclipse Luna IDE with Basis Development Tools (BDT) on Windows 8.1 was used.  The BBj runtime environment is 14.21.  The Java runtime environment is 8.0_40.

Following is a screen shot of the IDE that shows a few of the sample programs in the left column (BDT Navigator) and a Unit Test (bbjt file) in the foreground with results on the bottom.

![bbj_unit_test.png](https://bitbucket.org/repo/XKBRX8/images/383630051-bbj_unit_test.png)