REM /**
REM  * NamesDataAPI.bbj
REM  * @author S Swett
REM  * written 6/6/15
REM  */


rem *---------- CONSTANTS ----------*

rem  This section never runs for multi-entry point called programs, so subroutine "setup_constants" is called
rem  for each entry point.


rem *---------- FUNCTIONS ----------*


def fnGetPaddedName$(aName$)

    return pad(aName$, 15, "L", " ")

fnend


def fnGetPaddedShirtSize$(aShirtSize$)

    return pad(aShirtSize$, 4, "L", " ")

fnend



rem *---------- ENTRY POINTS ----------*


create_file_and_sample_data:

    enter
    
    gosub setup_constants
    
    gosub create_file_and_data

    exit


open_channel:

    enter fileChannelOut
    
    gosub setup_constants
    
    fileChannelOut = unt
    open(fileChannelOut) NAMES_FILE_NAME$
    
    exit
    
    
close_channel:

    enter fileChannelIn
    
    close(fileChannelIn)
    
    exit


get_dimensioned_record:

    enter recordOut$

    gosub setup_constants
    
    dim recordOut$:NAMES_TEMPLATE$
    
    exit
    

add_or_update_record:

    enter fileChannelIn, recordIn$, errMsgOut$
    
    gosub setup_constants
    
    errMsgOut$ = ""
    
    if len( cvs(recordIn.NAME$, 3) ) = 0 then 
:       errMsgOut$ = "NAME$::Name cannot be blank";
:       goto add_or_update_end
    
    if recordIn.GENDER$ <> "M" and recordIn.GENDER$ <> "F" then 
:       errMsgOut$ = "GENDER$::Gender must be M or F";
:       goto add_or_update_end
    
    if len( cvs(recordIn.PHONE$, 3) ) <> 10 then 
:       errMsgOut$ = "PHONE$::Must be 10 numbers/characters in phone number";
:       goto add_or_update_end
    
    trimmedSize$ = cvs(recordIn.SHIRT_SIZE$, 2)
    
    if trimmedSize$ <> "S" and trimmedSize$ <> "M" and trimmedSize$ <> "L" and trimmedSize$ <> "XL" and 
:       trimmedSize$ <> "XXL" and trimmedSize$ <> "XXXL" then 
:           errMsgOut$ = "SHIRT_SIZE$::Shirt size must be one of these: S M L XL XXL XXL";
:           goto add_or_update_end

        rem  Validation passed, so OK to write record
    
    write record(fileChannelIn) FIELD(recordIn$)
    
    add_or_update_end:

    exit


delete_record:
    
    enter fileChannelIn, keyIn$

    gosub setup_constants

    remove(fileChannelIn, key=keyIn$, dom=delete_record_not_found)
    
    delete_record_not_found:

    exit


get_a_record:

    enter fileChannel, useKeyIn, keyIn$, recordFoundOut, recordOut$
    
    gosub setup_constants
    
    dim recordOut$:NAMES_TEMPLATE$
    
    if useKeyIn then
:       read record(fileChannel, key=keyIn$, dom=no_names_data) recordOut$
:   else
:       read record(fileChannel, dom=no_names_data, end=no_names_data) recordOut$
    
    recordFoundOut = TRUE
    
    goto get_a_record_end 
    
    no_names_data:
    
    recordFoundOut = FALSE
    
    get_a_record_end: 

    exit


get_a_record_with_dir:

    enter fileChannel, useKeyIn, keyIn$, direction, recordFoundOut, recordOut$
    
    gosub setup_constants
    
    dim recordOut$:NAMES_TEMPLATE$
    
    if useKeyIn then
:       read record(fileChannel, key=keyIn$, dir=direction, dom=no_names_data_dir) recordOut$
:   else
:       read record(fileChannel, dir=direction, dom=no_names_data_dir, end=no_names_data_dir) recordOut$
    
    recordFoundOut = TRUE
    
    goto get_a_record_with_dir_end 
    
    no_names_data_dir:
    
    recordFoundOut = FALSE
    
    get_a_record_with_dir_end: 

    exit
    


rem *---------- SUBROUTINES ----------*


setup_constants:

    TRUE = 1
    FALSE = 0
    
    NAMES_FILE_NAME$ = "namesFile"
    
    NAMES_TEMPLATE$ = "NAME:C(15),GENDER:C(1),PHONE:C(10),SHIRT_SIZE:C(4)"
    
    return

    
create_file_and_data:

    rem     Name,  Gender,  Phone Number,  T-Shirt Size
    
    data "AMY", "F", "5551234568", "M" 
    data "ANDY", "M", "6298134578", "L"
    data "BARB", "F", "5551214578", "M"
    data "BETTY", "F", "5551236678", "S"
    data "CANDICE", "F", "5131245678", "M"
    data "CHERYL", "F", "2551235678", "M"
    data "DAVE", "M", "6298132578", "L"
    data "DOUG", "M", "6298134378", "L"
    data "ED", "M", "6298135578", "XL"
    data "EVELYN", "F", "5551295678", "S"
    
    data "FIONA", "F", "5551234678", "M"
    data "FRED", "M", "6291134578", "L"
    data "GRACE", "F", "5851234678", "M"
    data "GREG", "M", "6298135578", "XXL"
    data "HARRIET", "F", "5551245644", "S"
    data "HORACE", "M", "6228134578", "L"
    data "IRENE", "F", "9551234678", "M"
    data "IRMA", "F", "5751234578", "S"
    data "JACKIE", "F", "5551205678", "M"
    data "JOE", "M", "6298133578", "L"
    
    data "KAREN", "F", "5051234678", "S"
    data "KEITH", "M", "6798134578", "XXXL"
    data "LARRY", "M", "6292134578", "L"
    data "LAURA", "F", "8851234678", "M"
    data "MARY", "F", "5551234278", "S"
    data "MATT", "M", "6298134576", "L"
    data "NANCY", "F", "6291234678", "M"
    data "NORM", "M", "6248134578", "M"
    data "OLIVE", "F", "5551324978", "S"
    data "OTTO", "M", "6298134078", "L"
    
    data "PAT", "F", "5551234678", "M"
    data "PETE", "M", "6298134528", "XL"
    data "QUIN", "M", "6298114578", "XL"
    data "QUINCY", "F", "5551232678", "S"
    data "RHONDA", "F", "5451235678", "M"
    data "ROBERT", "M", "6298133578", "XL"
    data "SAM", "M", "6298130578", "L"
    data "SARAH", "F", "1512345378", "M"
    data "TIM", "M", "6238134578", "XL"
    data "TINA", "F", "5512245578", "S"
    
    data "URSULA", "F", "5512315678", "M"
    data "VICKY", "F", "7512345678", "M"
    data "VICTOR", "M", "6248134578", "XL"
    data "WENDY", "F", "2512345678", "S"
    data "WILL", "M", "6298184578", "L"
    data "YOLANDA", "F", "5512345578", "M"
    data "YVONNE", "F", "5552345178", "S"
    data "ZOEY", "F", "5551245648", "S"
    
    erase NAMES_FILE_NAME$,err=erase_problem
    
    erase_problem:
    
    mkeyed NAMES_FILE_NAME$, [1:1:15], 0, 64,err=create_problem
    
    create_problem:
    
    namesFileChannel = unt
    open(namesFileChannel) NAMES_FILE_NAME$
    dim record$:NAMES_TEMPLATE$
    
    read_data:
    
      dread name$, record.GENDER$, record.PHONE$, shirtSize$ , err=done_reading_data
      record.NAME$ = fnGetPaddedName$(name$)
      record.SHIRT_SIZE$ = fnGetPaddedShirtSize$(shirtSize$)
      
      write record(namesFileChannel) field(record$)
      goto read_data
    
    done_reading_data:

    return