

REM /**
REM  * ScreenIOWithInpute.bbj
REM  * @author Steve Swett
REM  * 
REM  * Rough draft: 5/28/15
REM  *
REM  */


REM *---------- CONSTANTS ----------*


TRUE = 1
FALSE = 0


    rem For function key programming, see the init subroutine.

REM     Note about INPUTE
REM     
REM     Any value less than 63 will cause the input function to exit setting the CTL value to the function value. The "help" function causes an exit with CTL=-1.
REM 
REM     When insert mode is toggled with CTL-T, SETOPTS byte 1 bit $20$ is also toggled.


F4_CUSTOM_INPUT_VALUE$ = chr(129)
F4_CTL_OUTPUT_VALUE$ = chr(4)

TAB_CUSTOM_INPUT_VALUE$ = chr(9)
TAB_CTL_OUTPUT_VALUE$ = chr(61)

BACKTAB_CUSTOM_INPUT_VALUE$ = chr(60)
BACKTAB_CTL_OUTPUT_VALUE$ = chr(62)

NUMBER_OF_FIELDS = 5

FIRST_NAME_CAPTION$ = "First name:"
FIRST_NAME_LENGTH = 15
FIRST_NAME_COL = 17
FIRST_NAME_ROW = 2
    
LAST_NAME_CAPTION$ = "Last name:"
LAST_NAME_LENGTH = 15
LAST_NAME_COL = 17
LAST_NAME_ROW = 3

GENDER_CAPTION$ = "Gender:"
GENDER_LENGTH = 1
GENDER_COL = 17
GENDER_ROW = 4

HAIR_COLOR_CAPTION$ = "Hair color:"
HAIR_COLOR_LENGTH = 10
HAIR_COLOR_COL = 17
HAIR_COLOR_ROW = 5

PHONE_CAPTION$ = "Phone#:"
PHONE_LENGTH = 13
PHONE_COL = 17
PHONE_ROW = 6
PHONE_MASK$ = "(000)000-0000"


REM *---------- FUNCTIONS ----------*


REM *---------- MAIN ROUTINE ----------*


gosub init

gosub load_fields_from_record

gosub display_fields

gosub edit_fields

eoj:

release

end


REM *---------- SUBROUTINES ----------*


    rem ------------------------------------------------------------------------------------------
    rem  One-time intialization
    rem ------------------------------------------------------------------------------------------

init:

    rem  The 'ET' (end type ahead control) below is VERY IMPORTANT when using SHIFT+TAB.  Without it, each
    rem  SHIFT+TAB would actually be treated as two inputs: SHIFT+TAB followed by TAB.

    print 'UC', 'CS', 'ET'
    
    rem Attempt to toggle Ctrl-T programmatically:
    
    rem turn on:
REM     a$ = opts
REM     A$(1,1)=IOR(A$(1,1),$20$)
REM     SETOPTS A$
    
    rem turn off: (seems to default to off)
REM     a$ = opts
REM     A$(1,1)=AND(A$(1,1), not($1F$))
REM     SETOPTS A$

    
    rem not sure whether this one or above turns off Byte 1, bit 1 options
REM     a$ = opts
REM     A$(1,1)=AND(A$(1,1), not($20$));   rem  the bitwise inverse (or bitwise logical not) of $20$ is $DF$
REM     SETOPTS A$


    
    
        rem For info on function key programming, see this article: http://www.basis.com/kb00025 
        rem Also see Basis help: PRO/5 > User's Ref > Character Devices > Terminals > Function and Edit Keys
     
        rem Below is how the 'FL' mnemonic is used to assign a function key a value
        
        rem "2" - code meaning to load only one key
        rem chr(3) - means the F4 key (0-based)
        rem chr(1) - means the custom value of the function key is 1 byte long
        rem F4_CUSTOM_INPUT_VALUE$ - is the custom INPUT value of the key
        
    print 'FL', "2", chr(3), chr(1), F4_CUSTOM_INPUT_VALUE$,
    
    
        rem Below is how to set the field terminator global string table so that F4 can be used as an "input" terminator
        
        rem F4_CUSTOM_INPUT_VALUE$ - must match the custom value of the key
        rem F4_CTL_OUTPUT_VALUE$ - the value you wish to be stored in the CTL variable when "input" termination occurs
    
    f4Pair$ = F4_CUSTOM_INPUT_VALUE$ + F4_CTL_OUTPUT_VALUE$

    
        rem Try adding the Tab key as a field terminator.  NOTE THE USE OF 'EL' VS. 'FL' MNEMONIC
        
    tabPair$ = TAB_CUSTOM_INPUT_VALUE$ + TAB_CTL_OUTPUT_VALUE$

    
        rem Try adding the Backtab key as a field terminator.  NOTE THE USE OF 'EL' VS. 'FL' MNEMONIC
        
    print 'EL', "2", chr(60), chr(1), BACKTAB_CUSTOM_INPUT_VALUE$,
        
    backtabPair$ = BACKTAB_CUSTOM_INPUT_VALUE$ + BACKTAB_CTL_OUTPUT_VALUE$

    resultIgnored$ = stbl("!EDIT", f4Pair$ + tabPair$ + backtabPair$ + stbl("!EDIT"))
    
    return
    

    rem ------------------------------------------------------------------------------------------
    rem  Pretend to load the fields from a data file record
    rem ------------------------------------------------------------------------------------------

load_fields_from_record:

        rem Pretend we're getting data from a file:
        
    recordDataFirstName$ = "STEVE"
    recordDataLastName$ = "SWETT"
    recordDataGender$ = "M"
    recordDataHairColor$ = "BLONDE"
    recordDataPhone = 5552786349

    firstName$ = pad(recordDataFirstName$, FIRST_NAME_LENGTH, "L", " ")
    lastName$ = pad(recordDataLastName$, LAST_NAME_LENGTH, "L", " ")
    gender$ = pad(recordDataGender$, GENDER_LENGTH, "L", " ")
    hairColor$ = pad(recordDataHairColor$, HAIR_COLOR_LENGTH, "L", " ")
    phone = recordDataPhone
    
    return
    
    
    rem ------------------------------------------------------------------------------------------
    rem  Display the fields on the screen
    rem ------------------------------------------------------------------------------------------

display_fields:

    print @(2,2), FIRST_NAME_CAPTION$
    print @(FIRST_NAME_COL, FIRST_NAME_ROW), 'BU', firstName$, 'EU'

    print @(2,3), LAST_NAME_CAPTION$
    print @(LAST_NAME_COL, LAST_NAME_ROW), 'BU', lastName$, 'EU'

    print @(2,4), GENDER_CAPTION$
    print @(GENDER_COL, GENDER_ROW), 'BU', gender$, 'EU'

    print @(2,5), HAIR_COLOR_CAPTION$
    print @(HAIR_COLOR_COL, HAIR_COLOR_ROW), 'BU', hairColor$, 'EU'

    print @(2,6), PHONE_CAPTION$
    print @(PHONE_COL, PHONE_ROW), 'BU', str(str(phone):PHONE_MASK$), 'EU'
    
    print @(2,9), "F4=Exit"
    
    return
    
    rem ------------------------------------------------------------------------------------------
    rem  Edit the fields, one at a time, until F4 is pressed
    rem ------------------------------------------------------------------------------------------

    
edit_fields:

    fieldNumber = 1
    
    while TRUE
    
        print @(2,12), 'CL', "Field number: ", fieldNumber
        rem print @(2,14), 'CL', "Field terminator: ", CTL
    
        switch fieldNumber
        
            case 1; gosub read_first_name; break
            case 2; gosub read_last_name; break
            case 3; gosub read_gender; break
            case 4; gosub read_hair_color; break
            case 5; gosub read_phone; break
        
        swend


        if CTL = ASC(F4_CTL_OUTPUT_VALUE$) then
:           gosub show_edited_values;
:           exitto done_reading_fields
:       else if CTL = ASC(TAB_CTL_OUTPUT_VALUE$) or CTL = 0 then
:           fieldNumber = fieldNumber + 1
:       else if CTL = ASC(BACKTAB_CTL_OUTPUT_VALUE$) then
:           fieldNumber = fieldNumber - 1

        
        if fieldNumber > NUMBER_OF_FIELDS then 
:           fieldNumber = 1
:       else if fieldNumber < 1 then
:           fieldNumber = NUMBER_OF_FIELDS
        
    
    wend
    
    done_reading_fields:
    
    return


    rem ------------------------------------------------------------------------------------------
    rem  Read the first name field
    rem ------------------------------------------------------------------------------------------

read_first_name:

    print @(FIRST_NAME_COL, FIRST_NAME_ROW), 'BU' 
    inpute FIRST_NAME_COL, FIRST_NAME_ROW, FIRST_NAME_LENGTH, "", firstName$ 
    print @(FIRST_NAME_COL + FIRST_NAME_LENGTH, FIRST_NAME_ROW), 'EU' 

    return


    rem ------------------------------------------------------------------------------------------
    rem  Read the last name field
    rem ------------------------------------------------------------------------------------------

read_last_name:

    print @(LAST_NAME_COL, LAST_NAME_ROW), 'BU' 
    inpute LAST_NAME_COL, LAST_NAME_ROW, LAST_NAME_LENGTH, "", lastName$ 
    print @(LAST_NAME_COL + LAST_NAME_LENGTH, LAST_NAME_ROW), 'EU' 

    return


    rem ------------------------------------------------------------------------------------------
    rem  Read the gender field
    rem ------------------------------------------------------------------------------------------

read_gender:

    print @(GENDER_COL, GENDER_ROW), 'BU' 
    inpute GENDER_COL, GENDER_ROW, GENDER_LENGTH, "", gender$ 
    print @(GENDER_COL + GENDER_LENGTH, GENDER_ROW), 'EU' 

    return


    rem ------------------------------------------------------------------------------------------
    rem  Read the hair color field
    rem ------------------------------------------------------------------------------------------

read_hair_color:

    print @(HAIR_COLOR_COL, HAIR_COLOR_ROW), 'BU' 
    inpute HAIR_COLOR_COL, HAIR_COLOR_ROW, HAIR_COLOR_LENGTH, "", hairColor$ 
    print @(HAIR_COLOR_COL + HAIR_COLOR_LENGTH, HAIR_COLOR_ROW), 'EU' 

    return


    rem ------------------------------------------------------------------------------------------
    rem  Read the phone field
    rem ------------------------------------------------------------------------------------------

read_phone:

    phoneStr$ = str(phone)
    
    print @(PHONE_COL, PHONE_ROW), 'BU' 
    inpute PHONE_COL, PHONE_ROW, PHONE_MASK$, "", phoneStr$ 
    print @(PHONE_COL + len(PHONE_MASK$), PHONE_ROW), 'EU' 
    
    phone = num(phoneStr$)

    return
    
    
    rem ------------------------------------------------------------------------------------------
    rem  Show all the fields (after editing)
    rem ------------------------------------------------------------------------------------------

show_edited_values:

        rem For the time being, BBj doesn't debug well if continuation lines are used

    print @(2,17), "Edited values (4 sec pause): ", 
:       firstName$, " - ", lastName$, " - ", gender$, " - ", hairColor$, " - ", str(str(phone):PHONE_MASK$)

    wait 4
    
    return
